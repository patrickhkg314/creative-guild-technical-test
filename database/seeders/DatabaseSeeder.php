<?php

namespace Database\Seeders;

use App\Models\Album;
use App\Models\Photographer;
use Exception;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(__DIR__ . "/../../landscapes.json");
        if ($jsonString === false) {
            // deal with error...
        }

        $json = json_decode($jsonString, TRUE);

        DB::beginTransaction();
        try {
            if (Photographer::count() >= 1) {
                throw new Exception("database have already been seeded. Stop seeding.");
            }

            $photographer = Photographer::create([
                'name' => $json['name'],
                'phone' => $json['phone'],
                'email' => $json['email'],
                'bio' => $json['bio'],
            ]);

            $photographer->profilePicture()->create([
                'url' => $json['profile_picture'],
            ]);

            foreach ($json['album'] as $v) {
                $album = Album::create([
                    'id' => $v['id'],
                    'title' => $v['title'],
                    'description' => $v['description'],
                    'date' => $v['date'],
                    'featured' => (boolean)$v['featured'],
                ]);

                $album->photographers()->attach($photographer);
                $album->image()->create([
                    'url' => $v['img'],
                ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::alert($e->getMessage());
        }
    }
}
