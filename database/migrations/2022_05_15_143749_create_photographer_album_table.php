<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_albums', function (Blueprint $table) {
            $table->id();
            $table->nullableMorphs('userable');
            $table->bigInteger('album_id')->nullable()->unsigned();
            $table->foreign('album_id')->references('id')->on('albums')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_albums', function (Blueprint $table) {
            $table->dropForeign('user_albums_album_id_foreign');
            $table->dropColumn('album_id');
        });

        Schema::dropIfExists('user_albums');
    }
};
