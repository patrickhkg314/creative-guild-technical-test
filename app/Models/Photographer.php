<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photographer extends User
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        "name", "phone", "email", "bio"
    ];

    public function albums()
    {
        return $this->morphToMany(Album::class, 'userable','user_albums')->withTimestamps();
    }

    /**
     * Get the post's image.
     */
    public function profilePicture()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
