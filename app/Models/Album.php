<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        "title", "description", "date", "featured"
    ];

    public function photographers()
    {
        return $this->morphedByMany(Photographer::class, 'userable','user_albums')->withTimestamps();
    }

    /**
     * Get the album's image.
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
