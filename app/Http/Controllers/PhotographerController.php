<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePhotographerRequest;
use App\Http\Requests\UpdatePhotographerRequest;
use App\Http\Resources\PhotographerCollection;
use App\Http\Resources\PhotographerResource;
use App\Models\Photographer;

class PhotographerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return PhotographerCollection
     */
    public function index()
    {
        return new PhotographerCollection(Photographer::paginate());
    }

    /**
     * Show the profile for a given user.
     *
     * @param Photographer $photographer
     * @return PhotographerResource
     */
    public function show(Photographer $photographer)
    {
        return new PhotographerResource($photographer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePhotographerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePhotographerRequest $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePhotographerRequest  $request
     * @param  \App\Models\Photographer  $photographer
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePhotographerRequest $request, Photographer $photographer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Photographer  $photographer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photographer $photographer)
    {
        //
    }
}
