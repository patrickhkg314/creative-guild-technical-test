# technical_test_backend

## Build Setup

Requirement:
1. Docker installed
2. Composer installed
3. PHP version >= 8.0 installed

```bash
# Step 1: install dependencies
$ composer install

# Step 2: create .env and copied from .env.example
# Remark: Change setting if you need to avoid port collision
$ cp .env.example .env

# Step 3: Start the server using docker and laravel sail
$ ./vendor/bin/sail up -d

# Step 4: migrate database (first time only)
# Remark: need to run this again if added migration files
$ ./vendor/bin/sail artisan migrate

# Step 5: seeding database (first time only)
$ ./vendor/bin/sail artisan db:seed
